'''extensions.py'''

import random

from scrapy import signals
from scrapy.mail import MailSender
from twisted.internet import reactor
from twisted.internet.task import deferLater


class MailerExtension(object):

    '''Send emails to preconfigured address on item scraped.'''

    def __init__(self, mailer, mail_to, min_delay, max_delay):
        '''Set mailer, mail to address and delay min/max interval.'''
        self.mailer = mailer
        self.mail_to = mail_to
        self.min_delay = min_delay
        self.max_delay = max_delay

    @classmethod
    def from_crawler(cls, crawler):
        '''Send mailer settings to constructor.'''
        ext = cls(
            MailSender.from_settings(crawler.settings),
            crawler.settings.get('MAIL_TO'),
            crawler.settings.get('MIN_DELAY'),
            crawler.settings.get('MAX_DELAY')
        )
        crawler.signals.connect(ext.mail, signal=signals.item_scraped)
        return ext

    @property
    def delay(self):
        '''Return random delay in seconds to avoid Google SMTP blocks.'''
        return random.randint(self.min_delay, self.max_delay)

    def mail(self, item ,*args, **kwargs):
        '''Send email with delay on item scraped.'''
        return deferLater(
            reactor,
            self.delay,
            self.mailer.send,
            mimetype='text/html',
            to=[self.mail_to],
            subject=item['title'],
            body=item['content']
        )
