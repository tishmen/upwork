'''pipelines.py'''

import txredisapi

from scrapy.exceptions import DropItem
from twisted.internet.defer import inlineCallbacks, returnValue


class DeduplicationPipeline(object):

    '''Redis based deduplication pipeline.'''

    def __init__(self, host, port, dbid):
        '''Set redis host, port and dbid.'''
        self.redis = txredisapi.lazyConnection(host, port, dbid)

    @classmethod
    def from_crawler(cls, crawler):
        '''Pass settings variables to constructor.'''
        return cls(
            crawler.settings.get('REDIS_HOST'),
            crawler.settings.get('REDIS_PORT'),
            crawler.settings.get('REDIS_DBID')
        )

    @inlineCallbacks
    def process_item(self, item, *args, **kwargs):
        '''Check if item exists in redis set by id. Drop if duplicate.'''
        value = yield self.redis.sadd('upwork', item['uid'])
        if not value:
            raise DropItem
        returnValue(item)
