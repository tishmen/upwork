'''settings.py'''

import os


BOT_NAME = 'upwork'
SPIDER_MODULES = ['upwork.spiders']
ITEM_PIPELINES = {'upwork.pipelines.DeduplicationPipeline': 100}
MYEXT_ENABLED = True
EXTENSIONS = {'upwork.extensions.MailerExtension': 200}

REDIS_HOST = os.getenv('REDIS_HOST')
REDIS_PORT = int(os.getenv('REDIS_PORT'))
REDIS_DBID = int(os.getenv('REDIS_DBID'))

SECURITY_TOKEN = os.getenv('SECURITY_TOKEN')
USER_ID = os.getenv('USER_ID')
ORGANIZATION_ID = os.getenv('ORGANIZATION_ID')

MAIL_HOST = os.getenv('MAIL_HOST')
MAIL_PORT = int(os.getenv('MAIL_PORT'))
MAIL_USER = os.getenv('MAIL_USER')
MAIL_PASS = os.getenv('MAIL_PASS')
MAIL_FROM = os.getenv('MAIL_USER')
MAIL_TO = os.getenv('MAIL_USER')
MIN_DELAY = 10
MAX_DELAY = 100
