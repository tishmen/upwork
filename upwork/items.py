'''items.py'''

from datetime import datetime

from scrapy import Item, Field
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose


def format_date(value):
    '''Convert date to ISO format'''
    return datetime.strptime(value, '%a, %d %b %Y %H:%M:%S +0000').isoformat()


class JobItem(Item):

    '''Upwork job item.'''

    uid = Field()
    url = Field()
    title = Field()
    content = Field()
    date = Field()


class JobItemLoader(ItemLoader):

    '''Upwork job item loader.'''

    default_item_class = JobItem
    default_input_processor = TakeFirst()
    default_output_processor = TakeFirst()
    uid_in = MapCompose(lambda x: x.split('?')[0].split('_%7E')[1])
    url_in = MapCompose(lambda x: x.split('?')[0])
    title_in = MapCompose(lambda x: x.split(' - Upwork')[0])
    date_in = MapCompose(format_date)
