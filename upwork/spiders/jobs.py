'''jobs.py'''

from scrapy import Request
from scrapy.exceptions import NotConfigured
from scrapy.spiders import XMLFeedSpider

from upwork.items import JobItemLoader


class JobsSpider(XMLFeedSpider):

    '''Upwork job spider.'''

    name = 'jobs'
    iterator = 'iternodes'
    itertag = 'item'

    def __init__(self, user_id, organization_id, security_token, *args,
                 **kwargs):
        '''Set user id, organization id and security token.'''
        super(JobsSpider, self).__init__(*args, **kwargs)
        if not (user_id and organization_id and security_token):
            raise NotConfigured
        self.user_id = user_id
        self.organization_id = organization_id
        self.security_token = security_token

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        '''Pass settings variables to constructor.'''
        return cls(
            crawler.settings.get('USER_ID'),
            crawler.settings.get('ORGANIZATION_ID'),
            crawler.settings.get('SECURITY_TOKEN')
        )

    def start_requests(self):
        '''Set useragent and get initial profiles page.'''
        url = 'https://www.upwork.com/ab/feed/topics/rss?userUid={}&orgUid={}&'\
            'securityToken={}'
        yield Request(
            url.format(self.user_id, self.organization_id, self.security_token)
        )

    def parse_node(self, response, selector):
        '''Parse single feed item.'''
        loader = JobItemLoader(selector=selector)
        loader.add_xpath('uid', 'link/text()')
        loader.add_xpath('url', 'link/text()')
        loader.add_xpath('title', 'title/text()')
        loader.add_xpath('content', 'description/text()')
        loader.add_xpath('date', 'pubDate/text()')
        yield loader.load_item()
